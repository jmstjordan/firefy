##Title
Firefy
##Author
JM Jordan
##What am I looking at
This is a real estate purchase and notification manager that runs as a web application.
Essentially, a user can save information about a real estate purchase, and set up notifications
regarding that purchase for later. For example, if I buy some real estate property I would
enter the address, purchase date, price, etc., and then based off those entries I would create
notifications to remind me to do various tasks from that purchase. The purchase information is stored
on Firebase realtime database, and the notifications use Google's Calendar API to access your calendar
and automatically set up notifications for your phone/desktop.
##How can I try this out?
Well, that's a little tricky because of the Firebase permissions. You could set up a Firebase account
and rip out my Firebase initialization config. Before that you can peek at what it looks like:

![picture](img/firefy-ss.png)

You'll notice on the left, are purchases you can select from. Calls for saving and deleting are asynchronous
so are very quick. You can cycle through old purchases on the left and each form populates immediately. On
the top you'll notice an event created after we filled out a notification bar, that is to say that an Event
was created on your (whoever logged in) Google Calendar. This app directly connects to it. You can also
sync your Google calendar to your phone calendar, so you'll receive phone notifications as well as an email.

##Technologies
This app is basically entirely front-end. It is hosted on Firebase and uses their NoSQL database. I also
used AngularJS for the client-side code, and have no server side code (I may use Firebase cloud functions when I get to that point). I used Bootstrap for my layout.

##Testing
End to End testing using Protractor.js. So far I am using simple functional tests to verify I can make reads
and writes to firebase.

##Installation
TODO: Add real instructions.

##What's next?
####For the user
- Email database for one-click email distribution
- Image uploading to each purchase
- Automatic accounting for aggregate of purchase forms
####For the developer
- Finish unit-level testing with karma/jasmine
- Installation instructions for interested users
