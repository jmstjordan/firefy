'use strict';
angular.module('firefy', [
    'ngRoute',
    'ngAnimate',
    'base.component',
    'base.controller',
    'purchase.component',
    'purchase.service',
    'purchase.controller',
    'datetime'
]);
