'use strict';

describe('angularjs purchase form creation', function() {
    it('should create a new purchase form and save it off to firebase', function(){
        browser.get('https://firefy-182ae.firebaseapp.com/');

        // smoke test for a valid page load
        expect(browser.getTitle()).toEqual('Welcome to Firebase Hosting');

        // signing in
        element(by.id('authorize-button')).click().then(function() {
            browser.driver.sleep(2000);
            var emailInput = browser.driver.findElement(by.id('identifierId'));
            emailInput.sendKeys('USER_HERE');

            browser.driver.findElement(by.id('identifierNext')).click();

            browser.driver.sleep(2000);

            var passwordInput = browser.driver.findElement(by.name('password'));
            passwordInput.sendKeys('PASSWORD_HERE');

            browser.driver.findElement(by.id('passwordNext')).click();

            // fill out form
            browser.driver.sleep(5000);
            element(by.id('address_id')).sendKeys('2730');
            element(by.id('street_id')).sendKeys('S. Wise Way');
            element(by.id('city_id')).sendKeys('Boise');
            element(by.id('state_id')).sendKeys('ID');
            element(by.id('zip_id')).sendKeys('83716');
            element(by.id('partner_id')).sendKeys('Benoit');
            element(by.id('purchase_price_id')).sendKeys('321098');
            element(by.id('purchase_date_id')).sendKeys('20120914');
            element(by.id('sale_price_id')).sendKeys('456789');
            element(by.id('sale_date_id')).sendKeys('20121014');
            element(by.id('HVAC_date')).sendKeys('20120914');
            element(by.id('save_purchase_id')).click();
            browser.driver.sleep(1000);

            // check event was created
            expect(browser.driver.findElement(by.id('content')).getText()).toContain('www.google.com')

            // get created purchase to delete
            expect(browser.driver.findElement(by.id('2730 S. Wise Way')).getText()).toEqual('2730 S. Wise Way');

            browser.driver.sleep(2000);

            browser.driver.findElement(by.id('2730 S. Wise Way')).click();
            element(by.id('delete_purchase_id')).click();

            browser.driver.sleep(2000);

            expect(element(by.id('2730 S. Wise Way')).isPresent()).toBe(false);

        });
    });
});
