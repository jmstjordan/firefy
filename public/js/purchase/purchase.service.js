'use strict';
angular.module('purchase.service', ['firebase']).service('AllPurchases', function($q, firebase){
    var ref = firebase.database().ref('purchases');
    this.getPurchases = function(){
        var deferred = $q.defer();
        var purchases = ref.once('value').then(function(snapshot){
            deferred.resolve(snapshot.val());
        }, function(error){
            deferred.reject(error);
        });
        return deferred.promise;
    };
});
