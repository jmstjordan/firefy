'use strict';
angular.module('purchase.component', ['purchase.controller']).component('purchase',{
    templateUrl: 'js/purchase/purchase.html',
    controller: 'PurchaseController'
});
