// Client ID and API key from the Developer Console
      var CLIENT_ID = '447580495497-m4u9fa9o83hirur7pepo7q9bkceoiehs.apps.googleusercontent.com';

      // Array of API discovery doc URLs for APIs used by the quickstart
      var DISCOVERY_DOCS = ["https://www.googleapis.com/discovery/v1/apis/calendar/v3/rest"];

      // Authorization scopes required by the API; multiple scopes can be
      // included, separated by spaces.
      var SCOPES = "https://www.googleapis.com/auth/calendar";

      var authorizeButton = document.getElementById('authorize-button');
      var signoutButton = document.getElementById('signout-button');
      var accessButton = document.getElementById('access-button');
      /**
       *  On load, called to load the auth2 library and API client library.
       */
      function handleClientLoad() {
        gapi.load('client:auth2', initClient);
      }

      /**
       *  Initializes the API client library and sets up sign-in state
       *  listeners.
       */
      function initClient() {
        gapi.client.init({
          discoveryDocs: DISCOVERY_DOCS,
          clientId: CLIENT_ID,
          scope: SCOPES
        }).then(function () {
          authorizeButton.onclick = handleAuthClick;
          signoutButton.onclick = handleSignoutClick;
        });
      }

      firefy.auth().getRedirectResult().then(function(result) {
          if (result.credential) {
            // This gives you a Google Access Token. You can use it to access the Google API.
            var token = result.credential.accessToken;
            // ...
            //console.log(token);
            gapi.auth2.setToken({
                access_token: token
            });
          }
          // The signed-in user info.
          var user = result.user;
          console.log('redirect,', user);

        }).catch(function(error) {
          // Handle Errors here.
          var errorCode = error.code;
          var errorMessage = error.message;
          // The email of the user's account used.
          var email = error.email;
          // The firebase.auth.AuthCredential type that was used.
          var credential = error.credential;
          // ...
        });

        firefy.auth().onAuthStateChanged(function(user) {
            console.log('auth changed: ', user);
            if(user){
                authorizeButton.style.display = 'none';
                signoutButton.style.display = 'block';
                document.getElementById("show-purchase").setAttribute("style", "display: block")
            }else{
                authorizeButton.style.display = 'block';
                signoutButton.style.display = 'none';
                document.getElementById("show-purchase").setAttribute("style", "display: none")
            }
        });

      function getToken(){
          var user = gapi.auth2.getAuthInstance().currentUser.get();
          return user.getAuthResponse().access_token;
      }

      function getFirebase(){
          return firefy;
      }

      function getGapiClientCalendar(){
          return gapi.client.calendar;
      }

      function executeRequest(request){
          request.execute(
              function(event) {
                  appendPre('Event created: ' + event.htmlLink);
              }
          );
      }

      /**
       *  Sign in the user upon button click.
       */
      function handleAuthClick(event) {
          var provider = new firebase.auth.GoogleAuthProvider();
          provider.addScope("https://www.googleapis.com/auth/calendar");
          firefy.auth().signInWithRedirect(provider);
      }

      /**
       *  Sign out the user upon button click.
       */
      function handleSignoutClick(event) {
        //console.log('sign out ');
        firefy.auth().signOut();
      }

      /**
       * Append a pre element to the body containing the given message
       * as its text node. Used to display the results of the API call.
       *
       * @param {string} message Text to be placed in pre element.
       */
      function appendPre(message) {
        var pre = document.getElementById('content');
        var textContent = document.createTextNode(message + '\n');
        pre.appendChild(textContent);
      }

      /**
       * Print the summary and start datetime/date of the next ten events in
       * the authorized user's calendar. If no events are found an
       * appropriate message is printed.
       */
      function listUpcomingEvents() {
        gapi.client.calendar.events.list({
          'calendarId': 'primary',
          'timeMin': (new Date()).toISOString(),
          'showDeleted': false,
          'singleEvents': true,
          'maxResults': 10,
          'orderBy': 'startTime'
        }).then(function(response) {
          var events = response.result.items;
          appendPre('Upcoming events:');

          if (events.length > 0) {
            for (i = 0; i < events.length; i++) {
              var event = events[i];
              var when = event.start.dateTime;
              if (!when) {
                when = event.start.date;
              }
              appendPre(event.summary + ' (' + when + ')')
            }
          } else {
            appendPre('No upcoming events found.');
          }
        });
      }
