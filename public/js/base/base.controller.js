'use strict';
angular.module('base.controller', ['firebase']).controller('BaseController', function BaseController($timeout, $route){

    var bc = this;

    bc.toggleSignIn = function(){
        var provider = new firebase.auth.GoogleAuthProvider();
        provider.addScope("https://www.googleapis.com/auth/calendar");

        if(firebase.auth().currentUser){
            firebase.auth().signOut();
        }else{
            firebase.auth().signInWithPopup(provider).then(
                function(result){
                    bc.token = result.credential.accessToken;
                    bc.user = result.user;
                    console.log('sign in with popup, ', result);
                    // $route.reload();
                },
                function(error){
                    console.log(error);
                }
            );
        }
    };

    /**
        Setting up listeners for auth changes, and a redirect after google sign in
    */
    // firebase.auth().onAuthStateChanged(function(user) {
    //     if(user){
    //         // trigger for reload waiting for slow google
    //         console.log('auth changed 1, ', user);
    //     }else{
    //         console.log('auth changed 2, ', user);
    //     }
    //     bc.user = user;
    // });
    // firebase.auth().getRedirectResult().then(
    //     function(result) {
    //         if (result.credential) {
    //         // This gives you a Google Access Token.
    //         var token = result.credential.accessToken;
    //         }
    //         var user = result.user;
    //         console.log('redirect, ', user);
    //
    //     },
    //     function(error){
    //         console.log('redirect error, ',  error);
    //     }
    // );
});
