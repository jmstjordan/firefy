'use strict';
angular.module('purchase.controller', ['firebase', 'purchase.service', 'datetime']).
controller('PurchaseController', function PurchaseController(AllPurchases, datetime, $timeout){

    var pc = this;

    pc.inNotifications = [
        "HVAC",
        "Gas",
        "Water",
        "Electric",
        "Hardwood",
        "Carpet",
        "Lockbox"
    ];

    pc.refresh = function(){
        location.reload();
    }

    pc.animate = function(message) {
        // TODO: Fix this, its a little clunky
        pc.message = message;
        pc.showMessage = true;

        // Simulate 1 seconds loading delay
        $timeout(function() {
            // Loading done here - Show message for .75 more second.
            $timeout(function() {
                pc.showMessage = false;
            }, 1000);
        }, 750);
    };

    pc.CurrentDate = new Date();

    pc.loadAll = function(key){
        AllPurchases.getPurchases().then(function(data) {
            // storing off they key so I can easily access individual purchases
            // feels kind of like a hack, but will do for now
            angular.forEach(data, function(v, k){
                v["key"] = k;
                // this is so we can make new notifications per purchase
                // without duplicating old ones
                v["notifications"] = {};
            });
            pc.purchases = data;
            pc.load(key);
        }, function(error) {
            console.error(error);
        });
    };

    pc.load = function(key) {
        if(key == null){
            // new form
            pc.purchase = {};
        }else{
            pc.purchase = pc.purchases[key];
        }
    };

    pc.save = function(){
        if(!validateSave(pc.purchase)){
            return null;
        }

        // note when we save the purchase, pc.purchase is the purchase we are loading
        // so we don't need to pass it in. In our load() function, we pass it in because
        // we are changing from pc.purchase to a different purchase

        var p = handleIncomingDates(pc.purchase);
        var localFire = getFirebase();
        //console.log('fire, ', localFire);
        if(p.key != null){
            // it is an existing purchase
            localFire.database().ref('purchases/' + p.key).set(p);
            pc.animate('Saved');
        }else{
            // it is a new purchase
            localFire.database().ref('purchases').push(p).then((snap) => {
                const key = snap.key;
                pc.loadAll(key);
                pc.animate('Saved');
            });
        }
        if(p.notifications != null){
            createNotifications(p.notifications);
        }
    };

    pc.delete = function(){
        var p = pc.purchase;

        if(p == null || p.key == null){
            return null;
        }
        var localFire = getFirebase();
        localFire.database().ref('purchases/' + p.key).remove().then(function(snap){
            pc.loadAll();
            pc.purchase = {};
            pc.animate('Deleted');
        }, function(errorObject){
            console.log("The write failed: " + errorObject.code);
        });
    };

    pc.loadAll(null);

    function createNotifications(notifications){
        angular.forEach(notifications, function(v, k){
            if(v.date != null){
                var startDate = v.date + 'T14:00:00.000Z';
                var endDate = v.date + 'T15:00:00.000Z';

                var message = "This is a reminder for this service";
                if(v.message != null){
                    message = v.message;
                }
                var count = 1;
                var frequency = "DAILY";
                if(v.frequency != null){
                    frequency = v.frequency;
                    count = 3;
                }
                // Note that method override reminders is minutes before event
                var event = {
                  'summary': k,
                  'description': message,
                  'start': {
                    'dateTime': startDate,
                    'timeZone': 'America/Los_Angeles'
                  },
                  'end': {
                    'dateTime': endDate,
                    'timeZone': 'America/Los_Angeles'
                  },
                  'recurrence': [
                    'RRULE:FREQ=' + frequency + ';COUNT=' + count
                  ],
                  'reminders': {
                    'useDefault': false,
                    'overrides': [
                      {'method': 'email', 'minutes': 1},
                      {'method': 'popup', 'minutes': 1}
                    ]
                  }
                };
                console.log(event);
                var calendar = getGapiClientCalendar();
                var request = calendar.events.insert({
                    'calendarId': 'primary',
                    'resource': event
                });
                executeRequest(request);
            }
        });
    };

    function validateSave(p){
        // TODO: add more validation than simple smoke test
        if(p == null || p.purchase_date == null || p.address_number == null || p.street == null){
            return false;
        }else{
            return true;
        }
    };

    function handleIncomingDates(purchase){

        var parser = datetime("yyyy-MM-dd");
        if(purchase.entry_date == null){
            purchase.entry_date = parser.setDate(new Date).getText();
        }
        // TODO: Refactor this. Currently if it's not a string, and it's not null, then
        // it must be a DateObject, as that is the default
        if(purchase.purchase_date != null && typeof purchase.purchase_date !== 'string'){
            purchase.purchase_date = parser.setDate(purchase.purchase_date).getText();
        }
        if(purchase.sale_date != null && typeof purchase.sale_date !== 'string'){
            purchase.sale_date = parser.setDate(purchase.sale_date).getText();
        }
        // Now the notification dates
        if(purchase.notifications != null){
            angular.forEach(purchase.notifications, function(v, k){
                if(v['date'] != null && typeof v['date'] !== 'string'){
                    purchase.notifications[k]['date'] = parser.setDate(v['date']).getText();
                }
            })
        }
        return purchase;
    };
});
